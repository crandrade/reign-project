import dotenv from 'dotenv';
import App from './App';
import mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import loggerMiddleware from './middlewares/logger';
import { Entry } from './interfaces/entries';
import EntryController from './controllers/EntryController';
import EntryModelSchema from './models/EntryModelSchema';
import EntryModel from './models/EntryModel';
import EntryRouter from './routes/EntryRouter';
import { EntryScraperJob } from './jobs/EntryScraperJob';
import cron from 'node-cron';
import morgan from 'morgan';

// tslint:disable:no-console

dotenv.config();
const mongoDbPath = process.env.MONGO_DB_PATH;
const scraperUrl = process.env.SCRAPER_URL;
const scraperSchedule = process.env.SCRAPER_SCHEDULE;
const scraperRetriesBeforeRunning = Number(process.env.SCRAPER_RETRIES);
const serverPort = Number(process.env.SERVER_PORT);
mongoose.connect(mongoDbPath, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
const db = mongoose.connection;
db.on('error', (error) => {
    console.error("Couldn't connect to database. Check database connection");
    console.error("Exit(1)");
    process.exit(1);
});
db.once('open', () => {
    console.error('Connected to database');
});

const entryModel = new EntryModel(new EntryModelSchema());
const entryController = new EntryController(entryModel);

const scraper = new EntryScraperJob(entryController, scraperUrl);
const scraperName = `[${scraper.constructor.name}]`;
scraper.on(scraper.statuses.start, () => {
    console.log(new Date(), scraperName, 'Job started');
}).on(scraper.statuses.success, () => {
    console.log(new Date(), scraperName, 'Job succeeded');
}).on(scraper.statuses.error, () => {
    console.error(new Date(), scraperName, 'Job failed');
});

scraper.scrapeEntriesRetry(scraperRetriesBeforeRunning)
    .catch((err: Error) => {
        console.log(err);
        return entryModel.all().then((res: Entry[]) => {
            if (res.length > 0) { return Promise.resolve(); }
            else {
                return Promise.reject(
                    new Error(`Empty database after ${scraperRetriesBeforeRunning} retries`)
                );
            }
        });
    }).catch((error: Error) => {
        console.log(`Couldn't bring first stack of entries before starting. Exit(1)`);
        console.log(JSON.stringify(error));
        return;
    })
    .finally(() => {
        cron.schedule(scraperSchedule, () => {
            return scraper.cronJob();
        });
        const app = new App({
            port: serverPort,
            middleWares: [
                bodyParser.json(),
                bodyParser.urlencoded({ extended: true }),
                loggerMiddleware,
                morgan('combined')
            ],
            routes: [
                new EntryRouter(entryController)
            ]
        });
        app.listen();
    });

