import { IBaseController } from "controllers/IBaseController";

interface Statuses {
    start: string,
    success: string,
    error: string
}
export default interface IJob {
    statuses: Statuses;
    cronJob(controller: IBaseController): void;
}