import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { Entry } from 'interfaces/entries';
import { IBaseController } from 'controllers/IBaseController'
import IJob from './IJob';
import { EventEmitter } from 'events';

export const program = "0 1 * * *"

interface ServerResponse {
    hits: RemoteEntry[],
    nbHits: number,
    page: number,
    nbPages: number,
    hitsPerPage: number,
    exhaustiveNbHits: boolean,
    query: string,
    params: string,
    processingTimeMS: number
}

interface RemoteEntry {
    objectID: string,
    created_at: Date,
    story_title?: string,
    title?: string,
    url?: string,
    story_url?: null,
    author: string,
    story_id: null
}

async function httpGet<T>(url: string): Promise<T> {
    return axios.request<T>({
        method: 'get',
        url,
    }).then((response: AxiosResponse<T>) => {
        return response.data
    }).catch((err: Error) => { return Promise.reject(err) });
}

export class EntryScraperJob extends EventEmitter implements IJob {
    private controller: IBaseController;
    private url: string;
    private available: boolean;
    public statuses = {
        start: "EntryScraperJobStart",
        success: "EntryScraperJobSuccess",
        error: "EntryScraperJobFailure",
    }
    constructor(controller: IBaseController, url: string) {
        super();
        this.controller = controller;
        this.url = url;
    }

    async cronJob(): Promise<void> {
        // tslint:disable:no-console
        this.emit(this.statuses.start);
        return this.scrapeEntries().then(() => {
            this.emit(this.statuses.success);
        }).catch(() => {
            this.emit(this.statuses.error)
        });
    }

    async scrapeEntriesRetry(retries: number): Promise<void> {
        if (retries <= 0) {
            return Promise.reject(new Error("Max retries reached"));
        } else {
            return this.scrapeEntries()
                .then(() => {
                    this.emit(this.statuses.success);
                    return Promise.resolve();
                })
                .catch((error: AxiosError) => {
                    return this.scrapeEntriesRetry(--retries)
                        .catch((err: Error) => {
                            this.emit(this.statuses.error);
                            return Promise.reject(err)
                        });
                }).catch((error: Error) => {
                    this.emit(this.statuses.error);
                    return Promise.reject(error);
                });
        }
    }
    validate(entry: RemoteEntry): boolean {
        if (!entry.objectID) return false;
        if (!entry.title && !entry.story_title) return false;
        else if (!entry.url && !entry.story_url) return false;
        else if (!entry.objectID) return false;
        else return true
    }

    EntryFromRemoteEntry(entry: RemoteEntry): Entry {
        return {
            objectID: entry.objectID,
            author: entry.author,
            created_at: entry.created_at,
            title: entry.story_title ? entry.story_title : entry.title,
            url: entry.story_url ? entry.story_url : entry.url,
        }
    }

    async scrapeEntries(): Promise<void> {
        const result = await httpGet<ServerResponse>(this.url);
        const remoteEntries: RemoteEntry[] = result.hits.filter(this.validate)
        const entries = remoteEntries.map(
            (entry: RemoteEntry, index: number) => { return this.EntryFromRemoteEntry(entry) }
        );
        return await this.controller.saveMultiple(entries).catch((err) => {
            return Promise.resolve();
        });
    }
}