export interface IEntry {
    objectID: string;
    author: string;
    created_at: Date,
    title: string,
    url: string,
    deleted: boolean,
}

export interface Entry {
    objectID: string;
    author: string;
    created_at: Date,
    title: string,
    url: string,
}

export function EntryFromIEntry(ientry: IEntry): Entry{
    return {
        objectID: ientry.objectID,
        author: ientry.author,
        created_at: ientry.created_at,
        title: ientry.title,
        url: ientry.url
    }
}

export function IEntryFromEntry(entry: Entry): IEntry{
    return {
        objectID: entry.objectID,
        author: entry.author,
        created_at: entry.created_at,
        title: entry.title,
        url: entry.url,
        deleted: false
    }
}

export interface IEntryQuery {
    objectID?: string;
}

export interface IEntryModelResponse {
    n: number,
    nModified: number,
    ok: boolean
}

export interface IEntryModel {
    all(): Promise<IEntry[]>;
    find(query: string): Promise<IEntry>;
    exists(query: string): Promise<boolean>;
    insertMultiple(entries: IEntry[]): Promise<void>;
    save(entry: IEntry): Promise<void>;
    delete(id: string): Promise<any>;
}