import express, { Application } from 'express';
import IRouter from './routes/IRouter';
import cors from 'cors';

export default class App {
    public app: Application;
    public port: number;

    constructor(appInit: { port: number; middleWares: any, routes: IRouter[] }) {
        this.app = express();
        this.port = appInit.port;
        this.middlewares(appInit.middleWares);
        this.app.use(cors());
        this.app.use(express.json());
        this.routers(appInit.routes);
    }

    private middlewares(middleWares: { forEach: (arg0: (middleWare: any) => void) => void; }) {
        middleWares.forEach(middleWare => {
            this.app.use(middleWare);
        });
    }

    private routers(routes: IRouter[]) {
        routes.forEach(route => {
            this.app.use('/api/v1', route.router);
        });
    }

    public listen() {
        return this.app.listen(this.port, () => {
            // tslint:disable-next-line:no-console
            console.log(`Server has started at http://localhost:${this.port}`);
        });
    }
}