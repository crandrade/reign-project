import { Router, Request, Response } from "express";
import {IEntry} from '../interfaces/entries';
import IRouter from './IRouter';
import { IBaseController } from "../controllers/IBaseController";

export default class EntryRouter implements IRouter {
    private controller: IBaseController;
    public router: Router
    public path: string

    constructor(controller: IBaseController){
        this.controller = controller;
        this.path = "/entries"
        this.router = this.routes(this.path);
    }

    public routes(path: string): Router {
        const router = Router();
        router.get(`${this.path}`, async (req: Request, res: Response, next: any) => {
            this.controller.getEntries().then((response: IEntry[]) => {
                res.json(response);
            });
        });
        router.get(`${this.path}/:id`, (req: Request, res: Response, next: any) => {
            const id = req.params.id
            this.controller.getEntry(id).then((response: IEntry) => {
                res.json(response);
            }).catch((err) => { res.status(404).json({error: true, message: err.message});});
        });
        router.delete(`${this.path}/:id`, (req: Request, res: Response, next: any) => {
            const id = req.params.id
            this.controller.deleteEntry(id).then(() => {
                res.status(200).json({});
            })
            .catch((err) => {
                res.status(500).json({error: true, message: err.message});});
        });
        return router;
    }
}
