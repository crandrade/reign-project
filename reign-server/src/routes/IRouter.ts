import {Router} from 'express';

export default interface IRouter {
    router: Router
    path: string
}