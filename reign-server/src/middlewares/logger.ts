import { Request, Response} from 'express';

const loggerMiddleware = (req: Request, resp: Response, next: any) => {
    // tslint:disable-next-line:no-console
    console.log('Request logged:', req.method, req.path);
    // tslint:disable-next-line:no-console
    console.log('Response for', req.path, ': [', resp.statusCode, ']: ', JSON.stringify(resp.json));
    next();
}

export default loggerMiddleware