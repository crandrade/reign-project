import {Entry} from '../interfaces/entries'

export interface IBaseController {
    getEntries(): Promise<Entry[]>;
    getEntry(id: string): Promise<Entry>;
    existsEntry(id: string): Promise<boolean>;
    saveEntry(entry: Entry): Promise<void>;
    saveMultiple(entries: Entry[]): Promise<void>;
    deleteEntry(id: string): Promise<void>;
}