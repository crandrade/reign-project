import { IEntry, Entry, IEntryModel, IEntryFromEntry, EntryFromIEntry } from '../interfaces/entries';
import { IBaseController } from './IBaseController';

export default class EntryController implements IBaseController {
    private EntryModel: IEntryModel;
    constructor(EntryModel: IEntryModel) {
        this.EntryModel = EntryModel;
    };

    async getEntries(): Promise<Entry[]> {
        return this.EntryModel.all().then((res: IEntry[]) => {
            return res.map((ientry: IEntry) => {
                return EntryFromIEntry(ientry);
            });
        });
    };

    async getEntry(id: string): Promise<Entry> {
        return this.EntryModel.find(id)
            .then((entry: IEntry) => {
                return EntryFromIEntry(entry);
            })
            .catch((err: Error) => { return Promise.reject(err); });
    };

    async existsEntry(id: string): Promise<boolean> {
        return this.EntryModel.exists(id);
    }

    async saveEntry(entry: Entry): Promise<void> {
        const iEntry = IEntryFromEntry(entry);
        return this.EntryModel.save(iEntry)
            .catch((err: Error) => { return Promise.reject(err); });
    };

    async saveMultiple(entries: Entry[]): Promise<void> {
        const iEntries: IEntry[] = entries.map((entry: Entry) => {return IEntryFromEntry(entry)});
        return this.EntryModel.insertMultiple(iEntries);
    }

    async deleteEntry(id: string): Promise<void> {
        return this.EntryModel.delete(id);
    };
}