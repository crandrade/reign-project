import mongoose, { Schema, Document, Model, model } from 'mongoose';
import { IEntry } from '../interfaces/entries';

const EntrySchema = new Schema({
    objectID: {
        type: String,
        unique: true,
    },
    author: String,
    story_id: Number,
    created_at: Date,
    title: String,
    body: String,
    story_url: String,
    url: String,
    deleted: Boolean,
}, {
    timestamps: true,
});

export default class EntryModelSchema {
    model = mongoose.model<
        IEntry & Document,
        Model<IEntry & Document>
    >('Entry', EntrySchema, "entries");
}