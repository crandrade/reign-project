import { IEntry, IEntryModel, IEntryModelResponse } from 'interfaces/entries';
import EntryModelSchema from './EntryModelSchema';
import { match } from 'assert';
import { isNull } from 'util';

export default class EntryModel implements IEntryModel {
    private schema: EntryModelSchema
    constructor(DB: EntryModelSchema) {
        this.schema = DB;
    };

    async all(): Promise<IEntry[]> {
        return this.schema.model.find({ deleted: false }).sort({ created_at: "desc" }).then((res: IEntry[]) => { return res; });
    };

    async find(id: string): Promise<IEntry> {
        return this.schema.model.findOne({ objectID: id, deleted: false }).then((res: IEntry) => {
            if (isNull(res)) return Promise.reject(new Error(`Entry ${id} not found`));
            return res;
        });
    };

    async exists(query: string): Promise<boolean> {
        return this.schema.model.exists({ objectID: query })
            .then((res) => { return res; })
            .catch((err: Error) => { return Promise.reject(err); });
    }

    async save(entry: IEntry): Promise<void> {
        const object = new this.schema.model(entry)
        return object.save().then(() => { return; })
            .catch(error => Promise.reject(error));
    };

    async insertMultiple(entries: IEntry[]): Promise<void> {
        return this.schema.model.insertMany(entries, { ordered: false })
            .then((res: IEntry[]) => {
                if (entries.length <= 0) return Promise.resolve();
                else if (res.length > 0) return Promise.resolve();
                else Promise.reject(new Error("Not enough saved entries"));
            })
            .catch((err) => Promise.reject(err))
    }

    async delete(id: string): Promise<void> {
        return this.schema.model.updateOne(
            { objectID: id, deleted: false },
            { $set: { "deleted": true } }
        ).then((doc: IEntryModelResponse) => {
            if (doc.ok && doc.n > 0) return;
            else return Promise.reject(new Error(`Entry ${id} not found`));
        }).catch((error: Error) => { return Promise.reject(error) });
    };
}