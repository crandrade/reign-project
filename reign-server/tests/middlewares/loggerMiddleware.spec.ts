import loggerMiddleware from '../../src/middlewares/logger';

describe('loggerMiddleware', () => {
    it('Should log the requests when asked', () => {
        const console = jest.spyOn(global.console, 'log');
        const mockRequest = {
            method: 'GET',
            path: '/',
            body: {
                hello: "hello"
            }
        } as any;
        const mockResponse = {
            json: {
                hello: "hello!"
            },
            statusCode: 200
        } as any
        const logger = loggerMiddleware(mockRequest, mockResponse, () => { return; });
        expect(console).toHaveBeenNthCalledWith(1, "Request logged:", mockRequest.method, mockRequest.path);
        expect(console).toHaveBeenNthCalledWith(
            2,
            "Response for",
            mockRequest.path,
            ': [', mockResponse.statusCode, ']: ',
            JSON.stringify(mockResponse.json)
        );
    })
});