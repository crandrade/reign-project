import { IEntry, Entry, EntryFromIEntry, IEntryFromEntry } from '../../src/interfaces/entries';
import "regenerator-runtime/runtime";
import { IEntryModel } from 'interfaces/entries';

let ientry: IEntry;
let entryModel: IEntryModel;
ientry = {
    objectID: "123",
    author: "Cristian",
    created_at: new Date(),
    title: "Title",
    url: "http://localhost:8080",
    deleted: false
};

describe('Entry model', () => {
    beforeAll(() => {
        entryModel = {
            async all(): Promise<IEntry[]> { return Promise.resolve([ientry]); },
            async find(query: string) {
                if (query === ientry.objectID) { return Promise.resolve(ientry); }
                else { return Promise.reject(new Error("Not found")); }
            },
            async save(e: IEntry) {
                if (e === ientry) { return Promise.resolve(); }
                else { Promise.reject(); }
            },
            async exists(query: string) { return Promise.resolve(true) },
            async insertMultiple(entries: IEntry[]) { return Promise.resolve(); },
            async delete(_: string) { return Promise.resolve(); }
        }
    });

    it("should return a list of entries", async () => {
        const response = await entryModel.all()
        expect(response).toContain(ientry);
    });
    it("should find an entry if id exists", async () => {
        const response = await entryModel.find(ientry.objectID)
        expect(response).toEqual(ientry);
    });
    it("should fail when the entity is not found", async () => {
        const response = await entryModel.find('0').catch((err: Error) => { return err.message; });
        expect(response).toEqual("Not found");
    })
    it("should delete when asked", async () => {
        const response = entryModel.delete(ientry.objectID).then(() => { return; });
        expect(response).resolves.not.toThrow();
    });
    it("should save when asked", async () => {
        const response = entryModel.save(ientry);
        expect(response).resolves.not.toThrow();
    });
    it("should insertMultiple when asked", async () => {
        const response = entryModel.insertMultiple([ientry]);
        expect(response).resolves.not.toThrow();
    });
});

describe("EntryFromIEntry", () => {
    it("Should transform an IEntry into an Entry without deleted parameter", () => {
        const entry: IEntry = {
            ...ientry,
        }
        delete entry.deleted
        const newEntry = EntryFromIEntry(ientry)
        expect(newEntry).toEqual(entry)
    });
});

describe("IEntryFromEntry", () => {
    it("Should transform an IEntry into an Entry with deleted === false", () => {
        const entry: Entry = {
            objectID: "123",
            author: "Cristian",
            created_at: ientry.created_at,
            title: "Title",
            url: "http://localhost:8080"
        }
        const newIEntry = IEntryFromEntry(entry)
        expect(newIEntry).toEqual(ientry)
    });
});