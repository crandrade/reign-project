import App from '../src/App';
import IRouter from '../src/routes/IRouter';
import { Router } from 'express';
import express from 'express';

describe('App', () => {
    it('Should index all the routers', async () => {
        const router: IRouter = new class RouterMock implements IRouter {
            path = '';
            router = Router();
        }();
        const mockedApplication = jest.spyOn(express.application, 'use');
        const app = new App({ port: 80, middleWares: [], routes: [router, router] });
        expect(mockedApplication).toHaveBeenNthCalledWith(3, '/api/v1', router.router);
        expect(mockedApplication).toHaveBeenNthCalledWith(4, '/api/v1', router.router);
        expect(mockedApplication).toHaveBeenCalledTimes(4);
    });
});