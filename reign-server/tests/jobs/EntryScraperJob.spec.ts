import { EntryScraperJob } from '../../src/jobs/EntryScraperJob';
import { IBaseController } from '../../src/controllers/IBaseController';
import { Entry } from '../../src/interfaces/entries';
import axios from 'axios';

const entry: Entry = {
    objectID: "123",
    author: "Cristian",
    created_at: new Date(),
    title: "This is a title",
    url: "http://localhost",
}
describe("EntryScraperJob", () => {
    it("Should call the api when summoned", async () => {
        const json = {
            hits: [{
                "created_at": "2020-03-18T04:10:42.000Z",
                "title": "Some title",
                "url": "http://localhost",
                "author": "nodesocket",
                "comment_text": "I had a similar idea, but doesn&#x27;t this mean they store and can view the content of form submissions?",
                "story_id": 22601960,
                "story_title": "Airform: Functional HTML forms for front-end developers",
                "story_url": "https://airform.io",
                "objectID": "22613541",
            }],
            nbHits: 1,
            page: 1,
            nbPages: 1,
            hitsPerPage: 1,
            exhaustiveNbHits: true,
            query: "string",
            params: "string",
            processingTimeMS: 1
        }
        const axiosMock = jest.spyOn(axios, 'request').mockReturnValue(Promise.resolve({data: json}))
        const controller = new class ControllerMock implements IBaseController {
            getEntries() { return Promise.resolve([]); }
            getEntry(_: string) { return Promise.resolve(entry); }
            saveEntry(_: Entry) { return Promise.resolve(); }
            existsEntry(_: string) { return Promise.resolve(false); }
            saveMultiple(_: Entry[]): Promise<void> {
                return Promise.resolve();
            }
            deleteEntry(_: string) { return Promise.resolve(); }
        }()
        const controllerSave = jest.spyOn(controller, 'saveMultiple')
        const call = new EntryScraperJob(controller, "")
        const validate = jest.spyOn(call, 'validate')
        const cronJob = expect(await call.cronJob()).not.toThrow;
        expect(axiosMock).toBeCalledTimes(1);
        expect(validate).toBeCalledTimes(1);
        expect(controllerSave).toBeCalledTimes(1);
    });
});