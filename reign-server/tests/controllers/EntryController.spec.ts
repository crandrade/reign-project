import EntryController from '../../src/controllers/EntryController';
import { IEntryModel, IEntry, EntryFromIEntry } from '../../src/interfaces/entries';

const iEntry: IEntry = {
    objectID: "123",
    author: "Cristian",
    created_at: new Date(),
    title: "This is a title",
    url: "http://localhost",
    deleted: false,
}
const entryModel: IEntryModel = {
    async all(): Promise<IEntry[]> { return Promise.resolve([iEntry]); },
    async find(query: string) {
        if (query === iEntry.objectID) { return Promise.resolve(iEntry); }
        else { return Promise.reject(new Error("Not found")); }
    },
    async save(e: IEntry) {
        if (e === iEntry) { return Promise.resolve(); }
        else { Promise.reject(); }
    },
    async exists(query: string) { return Promise.resolve(true) },
    async insertMultiple(entries: IEntry[]) { return Promise.resolve(); },
    async delete(_: string) { return Promise.resolve(); }
}
describe("EntryController", () => {
    describe("all()", () => {
        it("Should give all the entries that are saved in the Model", async () => {
            const controller = new EntryController(entryModel);
            const entry = EntryFromIEntry(iEntry);
            const res = controller.getEntries();
            expect(res).resolves.not.toThrow();
            expect(await res).toContainEqual(entry);
        });
        it("Should fail if the call to Model fails", async () => {
            const failedEntryModel = jest.spyOn(entryModel, 'all').mockRejectedValue(new Error("Couldn't fetch"))
            const controller = new EntryController(entryModel);
            const entry = EntryFromIEntry(iEntry);
            const res = controller.getEntries();
            expect(res).rejects.toThrow("Couldn't fetch");
        });
    });
    describe("getEntry()", () => {
        it("Should give 1 entriy that matches the objectID in the Model", async () => {
            const controller = new EntryController(entryModel);
            const entry = EntryFromIEntry(iEntry)
            const res = controller.getEntry("123")
            expect(res).resolves.not.toThrow();
            expect(await res).toEqual(entry);
        });
        it("Should fail if the id doesn't match anything Model", async () => {
            const controller = new EntryController(entryModel);
            const entry = EntryFromIEntry(iEntry)
            const res = controller.getEntry("1")
            expect(res).rejects.toThrow("Not found");
        });
        it("Should fail if the call to Model fails", async () => {
            const failedEntryModel = jest.spyOn(entryModel, 'find').mockRejectedValue(new Error("Couldn't fetch"))
            const controller = new EntryController(entryModel);
            const entry = EntryFromIEntry(iEntry);
            const res = controller.getEntries();
            expect(res).rejects.toThrow("Couldn't fetch");
        });
    });
});