module.exports = {
    presets: ['@babel/preset-typescript', '@babel/preset-env'],
    plugins: [
        '@babel/proposal-class-properties',
        '@babel/plugin-transform-runtime',
    ],
};
