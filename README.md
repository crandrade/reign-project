# Reign-Project
A multiproject for reading news related to NodeJS

## Dependencies
* NodeJS & NPM (package management)
* MongoDB (database)
* Express and Typescript (server)
* React and Typescript (client)
* Docker (deployment)
* Docker Toolbox (local machine)

## Deployment
Obtain the Docker Machine IP from
``` export DOCKER_MACHINE_IP=$(docker-machine ip) ```
Or replace manually the ${DOCKER_MACHINE_IP} variable from the docker-compose.yml file

Then, run:
``` docker-compose -f docker-compose.yml build ```
And finally:
``` docker-compose -f docker-compose.yml up ```

**  Beware of Docker server's IP management: for instance, Docker Toolbox gives the Docker Virtual Machine IP for containers. With this, the React App knows at building time where the Express server will be held 


## Dev deployment
In console N°1:
``` cd reign-server && npm install && npm run start:dev```
In console N°2:
``` cd reign-client && npm install && npm run start```
In console N°3:
``` sudo mongod --dbpath reign-db/ ```

## Test
``` cd reign server && npm run test:coverage ```

## Pending tasks
* More test coverage
* General mantainance
* Migrate scheduler code from server's index.ts to class Scheduler