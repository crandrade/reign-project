import React from 'react';
import './App.css';
import CustomTable, { Entry } from './Table';

interface AppProps {
    server: string;
}
interface State {
    isLoading: boolean;
    entries: Entry[];
    isError: boolean;
    errorMessage: string;
}

async function http<T>(request: RequestInfo): Promise<T> {
    const response = await fetch(request);
    const body = await response.json();
    return body;
}

class App extends React.Component<AppProps, State> {
    private server: string;

    constructor(props: AppProps) {
        super(props);
        this.server = props.server;
        this.state = {
            isLoading: true,
            entries: [],
            isError: false,
            errorMessage: '',
        };
    }

    async componentDidMount() {
        await http<Entry[]>(`${this.server}/api/v1/entries`)
            .then(data => {
                this.setState({
                    isLoading: false,
                    entries: data,
                });
            })
            .catch((error: Error) => {
                this.setState({
                    isLoading: false,
                    isError: true,
                    errorMessage: error.stack || '',
                });
            });
    }

    public render() {
        return (
            <div className="App">
                <header className="App-header">
                    <h1>HN Feed</h1>
                    <h3>We &lt;3 hacker news</h3>
                </header>
                <div>
                    <CustomTable
                        server={this.server}
                        isLoading={this.state.isLoading}
                        entries={this.state.entries}
                        isError={this.state.isError}
                        errorMessage={this.state.errorMessage}
                    />
                </div>
            </div>
        );
    }
}

export default App;
