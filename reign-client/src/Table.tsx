import React from 'react';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Delete from '@material-ui/icons/Delete';
import Moment from 'moment';
import { makeStyles } from '@material-ui/core/styles';

const message = 'Are you sure? This cannot be undone';
Moment.updateLocale('en', {
    calendar: {
        lastDay: '[Yesterday]',
        sameDay: 'HH:MM'
    }
});
async function deleteEntry(server: string, id: string) {
    const requestOptions = {
        method: 'DELETE'
    };
    fetch(`${server}/api/v1/entries/${id}`, requestOptions)
        .then(result => {
            window.location.reload();
        })
        .catch((error: Error) => {});
}

const useStyles = makeStyles({
    table: {
        minWidth: '100%'
    },
    row: {
        backgroundColor: '#fff',
        '&hover': {
            backgroundColor: '#fafafa'
        },
        cursor: 'pointer',
        fontSize: '13pt',
        border: '1px #ccc'
    },
    title: {
        font: '#333'
    },
    time: {
        font: '#333'
    },
    author: {
        color: '#999'
    },
    error: {
        color: 'red'
    },
    empty: {
        color: 'green'
    }
});

export interface Entry {
    objectID: string;
    title: string;
    author: string;
    body: string;
    created_at: Date;
    url: string;
}

interface CustomData {
    server: string;
    isLoading: boolean;
    entries: Entry[];
    isError: boolean;
    errorMessage: string;
}

const CustomTable = (data: CustomData) => {
    const classes = useStyles();
    if (data.isLoading) {
        return (
            <div className="loading">
                <p>Loading entries...</p>
            </div>
        );
    } else if (data.isError) {
        return (
            <div className="error">
                <p>
                    There's been an error fetching the entries:{' '}
                    {data.errorMessage}
                </p>
            </div>
        );
    } else if (data.entries.length === 0) {
        return <div className="empty">No entries to show</div>;
    } else {
        return (
            <TableContainer component={Paper}>
                <Table className={classes.table}>
                    <TableBody>
                        {data.entries.map((row: Entry) => (
                            <TableRow
                                className={classes.row}
                                hover
                                key={row.objectID}
                            >
                                <TableCell
                                    className={classes.title}
                                    scope="row"
                                    onClick={event => {
                                        window.open(row.url, '_blank');
                                    }}
                                >
                                    {row.title}
                                </TableCell>
                                <TableCell
                                    align="right"
                                    className={classes.author}
                                    onClick={event => {
                                        window.open(row.url, '_blank');
                                    }}
                                >
                                    -{row.author}-
                                </TableCell>
                                <TableCell
                                    align="right"
                                    onClick={event => {
                                        window.open(row.url, '_blank');
                                    }}
                                >
                                    {Moment(row.created_at).calendar()}
                                </TableCell>
                                <TableCell align="right">
                                    <Delete
                                        onClick={event => {
                                            if (window.confirm(message)) {
                                                deleteEntry(
                                                    data.server,
                                                    row.objectID
                                                );
                                            }
                                        }}
                                    />
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </TableContainer>
        );
    }
};

export default CustomTable;
