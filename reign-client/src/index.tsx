import React from 'react';
import dotenv from 'dotenv';
import { render } from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

dotenv.config();
const backend_server = process.env.REACT_APP_BACKEND_SERVER || 'http://localhost:8080';

render(<App server={backend_server} />, document.getElementById('root'));
serviceWorker.unregister();
